Feature: Circles Life Login

  Scenario: Login with valid password
    Given I open Circles life login page
    When I login in valid credentials
    Then I will be logged in and redirect to plan page successfully

    When I navigate to My Account menu
    Then I should see my email correctly
