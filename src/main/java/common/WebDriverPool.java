package common;

import org.openqa.selenium.WebDriver;

public class WebDriverPool {

    public static WebDriver webDriver;

    public static void setWebDriver(WebDriver browser) {
        webDriver = browser;
    }

    public static WebDriver getWebDriver() {
        return webDriver;
    }

}
