package pages.circleslife;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class ShopBasePage extends BasePage {

    @FindBy(css = "div.st-content a[href*='/my_profile']")
    private WebElement myAccountLink;

    public ShopBasePage() {
        PageFactory.initElements(browser, this);
    }

    public void navigateToMyAccount() {
        myAccountLink.click();
    }
}
