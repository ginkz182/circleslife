package pages.circleslife;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;

public class PlanPage extends ShopBasePage {

    static String PAGE_TITLE = "The Best No-contract Mobile Plan";

    @FindBy(id = "checkout-title")
    private WebElement pageTitle;

    public PlanPage() {
        PageFactory.initElements(browser, this);
    }

    public void verifyPlanPageShowCorrectly() {
        waitForElementVisibility(pageTitle);
        assertEquals("Plan Page title is not show as expected", PAGE_TITLE, pageTitle.getText());
    }

}
