package pages.circleslife;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class HomePage extends BasePage {

    static String HOMEPAGE_URL = "http://circles.life/";

    @FindBy(css = "div.mw9.center a.blue.dim")
    private WebElement buyButton;

    public HomePage() {
        PageFactory.initElements(browser, this);
    }

    public void navigateToHomePage() {
        browser.get(HOMEPAGE_URL);
    }

    public void clickBuyButton() {
        buyButton.click();
    }
}
