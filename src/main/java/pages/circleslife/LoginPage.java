package pages.circleslife;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class LoginPage extends BasePage {

    @FindBy(css = "form.form-signin button.btn.btn-primary.btn-lg.btn-block.Button")
    private WebElement signinButton;

    @FindBy(css = "form.form-signin input[name='email']")
    private WebElement emailTextInput;

    @FindBy(css = "form.form-signin input[name='password']")
    private WebElement passwordTextInput;

    public LoginPage() {
        PageFactory.initElements(browser, this);
    }

    public void waitPageReady() {
        waitForElementVisibility(signinButton);
    }

    public void typeEmailAddress(String email) {
        emailTextInput.sendKeys(email);
    }

    public void typePassword(String password) {
        passwordTextInput.sendKeys(password);
    }

    public void clickSigninButton() {
        signinButton.click();
    }

    public void loginWithUserAndPassword(String email, String password) {
        typeEmailAddress(email);
        typePassword(password);
        clickSigninButton();
    }
}
