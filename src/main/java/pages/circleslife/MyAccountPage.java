package pages.circleslife;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;

public class MyAccountPage extends ShopBasePage {

    @FindBy(xpath = "//label[text()='Email']/..//input")
    private WebElement emailTextInput;

    public MyAccountPage() {
        PageFactory.initElements(browser, this);
    }

    public String getEmail() {
        return emailTextInput.getAttribute("value");
    }

    public void verifyEmailAddress(String email) {
        waitForElementVisibility(emailTextInput);
        assertEquals("Email address is not as expected",email, getEmail());
    }
}
