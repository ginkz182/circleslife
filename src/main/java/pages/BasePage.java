package pages;

import common.WebDriverPool;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver browser;
    WebDriverWait wait;

    public BasePage() {
        browser = WebDriverPool.getWebDriver();
        wait = new WebDriverWait(browser, 15);
    }

    public void waitForElementVisibility(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

}
