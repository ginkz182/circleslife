package pages.facebook;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class FacebookLoginPage extends BasePage {

    static String FACEBOOK_URL = "http://www.facebook.com";

    @FindBy(id = "email")
    private WebElement emailInputText;

    @FindBy(id = "pass")
    private WebElement passwordInputText;

    @FindBy(css = "label#loginbutton input")
    private WebElement loginButton;

    public FacebookLoginPage() {
        PageFactory.initElements(browser, this);
    }

    public void typeEmail(String email) {
        emailInputText.sendKeys(email);
    }

    public void typePassword(String password) {
        passwordInputText.sendKeys(password);
    }

    public void clickLogin() {
        loginButton.click();
    }

    public void navigateToFacebook() {
        browser.get(FACEBOOK_URL);
    }

    public void loginWithFBCredentials(String email, String password) {
        typeEmail(email);
        typePassword(password);
        clickLogin();
    }

}
