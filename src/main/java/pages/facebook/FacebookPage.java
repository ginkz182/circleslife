package pages.facebook;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.BasePage;

public class FacebookPage extends BasePage {

    @FindBy(xpath = "//div[contains(text(), \"What's on your mind\")]")
    private WebElement statusBox;

    @FindBy(css = "div#u_0_15")
    private WebElement privacySetting;

    @FindBy(xpath = "//span[text() = 'More...']")
    private WebElement morePrivacySetting;

    @FindBy(xpath = "//div[text() = 'Only me']")
    private WebElement onlyMeOption;

    public FacebookPage() {
        PageFactory.initElements(browser, this);
    }

}
