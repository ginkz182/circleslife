package steps.facebook;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.facebook.FacebookLoginPage;
import pages.facebook.FacebookPage;

public class FacebookSteps {

    static String FB_LOGIN = "facebook@login.com";
    static String FB_PASSWORD = "password";

    FacebookLoginPage facebookLoginPage = new FacebookLoginPage();
    FacebookPage facebookPage = new FacebookPage();


    @Given("^I login to Facebook")
    public void loginToFaceBook() {
        facebookLoginPage.navigateToFacebook();
        facebookLoginPage.loginWithFBCredentials(FB_LOGIN, FB_PASSWORD);
    }

}
