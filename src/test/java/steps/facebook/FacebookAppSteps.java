import org.junit.Before;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class FacebookAppSteps {

    public AppiumDriver<MobileElement> driver;
    public WebDriverWait wait;

    @Before
    public void setup() throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "P10");
        caps.setCapability("udid", "6PM7N17325000855");
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "8.0");
        caps.setCapability("skipUnlock","true");
        caps.setCapability("appPackage", "com.facebook.katana");
        caps.setCapability("appActivity","com.facebook.katana.LoginActivity");
        caps.setCapability("noReset","false");

        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void testAndroid() {

    }
}
