package steps;


import common.WebDriverPool;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {

    @After
    public void afterScenario() {
        WebDriverPool.getWebDriver().quit();
    }

    @Before
    public void beforeScenario() {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
        WebDriver browser = new ChromeDriver();

        WebDriverPool.setWebDriver(browser);
    }
}
