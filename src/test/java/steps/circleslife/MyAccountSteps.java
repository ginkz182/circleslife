package steps.circleslife;

import pages.circleslife.MyAccountPage;
import common.TestData;
import cucumber.api.java.en.Then;

public class MyAccountSteps {

    MyAccountPage myAccountPage = new MyAccountPage();

    @Then("^I should see my email correctly")
    public void verifyEmailAddress() {
        String expectedEmail = TestData.getEmailID();
        myAccountPage.verifyEmailAddress(expectedEmail);
    }

}
