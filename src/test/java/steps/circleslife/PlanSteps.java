package steps.circleslife;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import pages.circleslife.PlanPage;

public class PlanSteps {

    PlanPage planPage = new PlanPage();

    @Then("^I will be logged in and redirect to plan page successfully$")
    public void iLoginInValidCredentials() {
        planPage.verifyPlanPageShowCorrectly();
    }

    @When("^I navigate to My Account menu$")
    public void navigateToMyAccount() {
        planPage.navigateToMyAccount();
    }
}
