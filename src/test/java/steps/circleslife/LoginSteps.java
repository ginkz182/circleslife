package steps.circleslife;

import common.TestData;
import cucumber.api.java.en.When;

import pages.circleslife.LoginPage;


public class LoginSteps {

    static String VALID_LOGIN = "test@account.com";
    static String VALID_PASSWORD = "Test1234";

    LoginPage loginPage = new LoginPage();

    @When("^I login in valid credentials$")
    public void iLoginInValidCredentials() {
        loginPage.waitPageReady();
        loginPage.loginWithUserAndPassword(VALID_LOGIN, VALID_PASSWORD);

        TestData.setEmailID(VALID_LOGIN);
    }
}
