package steps.circleslife;

import pages.circleslife.HomePage;
import cucumber.api.java.en.Given;

public class HomepageSteps {

    HomePage homePage = new HomePage();

    @Given("^I open Circles life login page")
    public void i_open_Circles_life_loginpage() {
        homePage.navigateToHomePage();
        homePage.clickBuyButton();
    }
}
